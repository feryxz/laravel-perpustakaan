<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    protected $guarded = [];

    public function siswas()
    {
        return $this->hasMany('App\Siswa');
    }

    public function bukus()
    {
        return $this->hasMany('App\buku');
    }
}
