<?php
function rupiah($angka)
{

    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
@extends('backend.master')
@section('title', 'Pinjaman Baru')
@section('sub-judul', 'Mahasiswa')

@section('button')

@endsection
@section('content')
@if(count($errors) >0)
@foreach($errors->all() as $error )
<div class="alert alert-danger" role="alert">
    {{ $error }}
</div>
@endforeach
@endif

@if(Session::has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ Session('success') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="row">
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">

                <h4 class="mt-0 header-title">Pinjaman Baru</h4>

                <p>
                    <a class="btn btn-warning btn-sm waves-effect waves-light" style="float: right!important;" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        -
                    </a>
                </p>
                <br><br>
                <div class="collapse show" id="collapseExample">
                    <div class="card card-body mb-0">
                        <form action="{{route('transaksi.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Mahasiswa</label>
                                <select class="form-control" name="siswa" required="">
                                    <option selected="" disabled="">-- Pilih Mahasiswa --</option>
                                    @foreach($siswa as $b)
                                    <option value="{{$b->id}}">{{$b->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Buku</label>
                                <select class="form-control" name="buku" required="">
                                    <option selected="" disabled="">-- Pilih Buku --</option>
                                    @foreach($buku as $b)
                                    <option value="{{$b->id}}">{{$b->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="box-footer">
                                <input type="submit" class="btn btn-primary pull-right" value="Tambah Transaksi">
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div>
<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            saldo: 0
        }
    })
</script>
<script type="text/javascript">
    Vue.filter('currency', function(val) {
        return accounting.formatMoney(val, "Rp. ", 0, ".", ",");;
    })
</script>

@endsection
@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('dist/plugins/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/plugins/datatables/buttons.bootstrap4.min.css') }}" />
<!-- Responsive datatable examples -->
<link href="{{ asset('dist/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('dist/assets/js/vue.js') }}"></script>
<script src="{{ asset('dist/assets/js/accounting.min.js') }}"></script>


<script src="{{ asset('dist/assets/js/vue.js') }}"></script>
<script src="{{ asset('dist/assets/js/accounting.min.js') }}"></script>

<style type="text/css">
    .noline {
        display: inline;
        margin: 0px;
        padding: 0px;
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
@endsection

@section('page_js')
<!-- DataTables -->
<!-- Required datatable js -->
<script src="{{ asset('dist/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('dist/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('dist/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('dist/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="assets/pages/datatables.init.js"></script>
<script>
    $(function() {
        $("#datatable").DataTable();
    });
</script>
@endsection