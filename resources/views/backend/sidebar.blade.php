<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Main</li>
                <li>
                    <a href="/" class="waves-effect">
                        <i class="mdi mdi-home-variant"></i><span> Dashboard </span>
                    </a>
                </li>

                <li class="menu-title">Components</li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-briefcase-check"></i> <span> Pinjaman <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                    <ul class="submenu">
                        <li><a href="{{route('pinjaman.create')}}">Tambah Pinjaman</a></li>
                        <li><a href="{{route('pinjaman.index')}}">Riwayat Pinjaman</a></li>
                        <!-- <li><a href="{{route('transaksi.rekap')}}">Rekap Transaksi</a></li> -->
                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-buffer"></i> <span> Buku <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                    <ul class="submenu">
                        <li><a href="/buku">List Buku</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-human-child"></i> <span> Mahasiswa <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                    <ul class="submenu">
                        <li><a href="/siswa">List Mahasiswa</a></li>
                    </ul>
                </li>


                <li class="menu-title">Extras</li>

            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->